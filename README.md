# BGU Assignment Template - LaTeX

This repo provides template files for BGU homework and assignments.

The template looks like this:


![Template example](./template.png)

(You can also find a PDF example on top of this page)

## Bonus
You can also use the YAML configuration to build and deploy your LaTeX process! On every commit, the CI/CD will build your PDF and upload it to GitLab pages!

## Overleaf link
[Overleaf template](https://www.overleaf.com/latex/templates/bgu-assignment-template/gbhmjkrcpbmn)
